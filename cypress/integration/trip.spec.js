describe('template spec', () => {
  it('passes', () => {
    cy.visit('https://ticket-eight-sigma.vercel.app/')
    cy.get('#departure').type('Paris')
    cy.get('#arrival').type('Bruxelles')
    cy.get('#date').type('2024-02-07')
    cy.get('#search').click()
  })
})